#include <iostream>
#include <cmath>

using std::cout;
using std::endl;

class KompleksniBroj {
public:
  double getRealni() {
    return realni;
  }
  void setRealni(double realni) {
    this->realni = realni;
  }
  double getImaginarni() {
    return imaginarni;
  }
  void setImaginarni(double imaginarni) {
    this->imaginarni = imaginarni;
  }
  double izracunajApsolutnuVrijednost() {
    return sqrt(pow(realni, 2) + pow(imaginarni, 2));
  }
  void prikaziKompleksniBroj() {
    std::cout << "Re: " << realni << ", ";
    std::cout << "Im: " << imaginarni << std::endl;
  }
  void zbrojiKompleksniBroj(KompleksniBroj broj) {
    this->realni = this->realni + broj.realni;
    this->imaginarni = this->imaginarni + broj.imaginarni;
  }

private:
  double realni;
  double imaginarni;
};

int main() {
  KompleksniBroj broj1;
  broj1.setRealni(2.3);
  broj1.setImaginarni(4.1);

  broj1.prikaziKompleksniBroj();
  double apsolutno = broj1.izracunajApsolutnuVrijednost();
  cout << "Apsolutna vrijednost: " << apsolutno << endl;

  KompleksniBroj broj2;
  broj2.setRealni(1.5);
  broj2.setImaginarni(3.7);

  broj1.zbrojiKompleksniBroj(broj2);
  broj1.prikaziKompleksniBroj();
}