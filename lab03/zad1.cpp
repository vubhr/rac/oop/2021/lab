#include <iostream>

using std::cout;
using std::endl;

struct KompleksniBroj {
  double realni;
  double imaginarni;
};

void prikaziKompleksniBroj(KompleksniBroj broj) {
  cout << "Re: " << broj.realni << ", ";
  cout << "Im: " << broj.imaginarni << endl;
}

KompleksniBroj zbrojiKompleksneBrojeve(KompleksniBroj broj1, KompleksniBroj broj2) {
  KompleksniBroj rezultat;
  rezultat.realni = broj1. realni + broj2.realni;
  rezultat.imaginarni = broj1.imaginarni + broj2.imaginarni;
  return rezultat;
}

int main() {
  KompleksniBroj broj1 { 23.3, 67.2 };
  KompleksniBroj broj2 { 13.2, -2.3 };  
  prikaziKompleksniBroj(broj1);

  KompleksniBroj zbroj = zbrojiKompleksneBrojeve(broj1, broj2);
  prikaziKompleksniBroj(zbroj);
}