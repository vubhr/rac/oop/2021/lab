#include <string>

class Osoba {
public:
  Osoba(std::string ime, std::string prezime);

  std::string getIme();
  std::string getPrezime();

private:
  std::string ime;
  std::string prezime;
};