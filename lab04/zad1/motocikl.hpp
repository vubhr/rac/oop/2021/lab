#include <string>

class Motocikl {
public:
  Motocikl();
  Motocikl(std::string proizvodac, std::string model);
  Motocikl(std::string proizvodac, std::string model, int zapremnina);

  void prikaz();

private:
  std::string proizvodac;
  std::string model;
  int zapremnina;
};