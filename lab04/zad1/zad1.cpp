#include "motocikl.hpp"

int main() {
  Motocikl motor1;
  motor1.prikaz();

  Motocikl motor2("Kawasaki", "Z300", 300);
  motor2.prikaz();

  Motocikl motor3("Yamaha", "MT-07");
  motor3.prikaz();
}