#include <iostream>
#include "valuta.hpp"

Valuta::Valuta(std::string naziv) :
  Valuta(naziv, 1.0) { }

Valuta::Valuta(std::string naziv, double tecaj) : naziv(naziv) {
  setTecaj(tecaj);
}

void Valuta::setTecaj(double tecaj) {
  if (tecaj > 0.0) {
    this->tecaj = tecaj;
  }
}

double Valuta::kupi(double kune) {
  return kune / tecaj;
}
