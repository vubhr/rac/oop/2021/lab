#include "web_stranica.hpp"

class WebPreglednik {
public:
  WebPreglednik();
  WebPreglednik(std::string url);
  WebPreglednik(WebStranica webStranica);

  void instalirajProsirenje();
  void prikazi();

private:
  WebStranica stranica;
  int brojProsirenja;
};