#include <iostream>
#include "web_preglednik.hpp"

WebPreglednik::WebPreglednik()
  : stranica("www.vub.hr"), brojProsirenja(0) {
}

WebPreglednik::WebPreglednik(std::string url) 
  : stranica(url), brojProsirenja(0) {
}

WebPreglednik::WebPreglednik(WebStranica str)
  : stranica(str), brojProsirenja(0) {
}

void WebPreglednik::instalirajProsirenje() {
  brojProsirenja++;
}

void WebPreglednik::prikazi() {
  std::cout << "Otvorena stranica: ";
  stranica.prikazi();
  std::cout << "Broj prosirenja: " << brojProsirenja << std::endl;
}