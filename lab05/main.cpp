#include <iostream>
#include "web_preglednik.hpp"

int main() {
  WebStranica stranica("www.google.com");

  WebPreglednik chrome;
  chrome.prikazi();

  WebPreglednik opera("www.facebook.com");
  opera.instalirajProsirenje();
  opera.instalirajProsirenje();
  opera.instalirajProsirenje();
  opera.prikazi();
}