#include <string>

class WebStranica {
public:
  WebStranica(std::string url, int port = 443, bool sigurno = true);
  void prikazi();

private:
  std::string url;
  int port;
  bool sigurno;
};