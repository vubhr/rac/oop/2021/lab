#include <iostream>

#define N 5

int prikaziPolje(int polje[], int n, int *prosjek = nullptr, int *maksimum = nullptr) {
  int brOperacija = 0;

  if (prosjek != nullptr) {
    *prosjek = 0;
    brOperacija++;
  }
  if (maksimum != nullptr) {
    *maksimum = polje[0];
    brOperacija++;
  }

  for (int i = 0; i < n; i++) {
    std::cout << polje[i] << " ";

    if (prosjek != nullptr) {
      *prosjek = *prosjek + polje[i];
    }
    if (maksimum != nullptr) {
      if (polje[i] > *maksimum) {
        *maksimum = polje[i];
      }
    }
  }
  std::cout << std::endl;

  if (prosjek != nullptr) {
    *prosjek = *prosjek / n;
  }

  return brOperacija;
}

int main() {
  int polje[5] = { 9, 4, 2, 6, 5 };
  int prosjek;
  int maksimum;

  prikaziPolje(polje, N);
  prikaziPolje(polje, N, &prosjek);
  prikaziPolje(polje, N, &prosjek, &maksimum);
  prikaziPolje(polje, N, nullptr, &maksimum);

  std::cout << "Prosjek: " << prosjek << std::endl;
  std::cout << "Maksimum: " << maksimum << std::endl;
}