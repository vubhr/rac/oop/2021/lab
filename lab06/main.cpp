#include "TownCenter.hpp"

int main() {
  Villager firstVillager;

  firstVillager.info();

  Villager second(12);
  second.info();

  Villager v(10, 2, 0.5);
  v.setHitPoints(11);
  v.info();

  TownCenter townCenter;
  Villager *villager = townCenter.trainVillager();
  villager->info();

  Outpost *outpost1 = townCenter.buildOutpost();
  outpost1->info();

  Outpost *outpost2 = townCenter.buildOutpost(villager);
  outpost2->info();

  delete outpost1;
  delete outpost2;
}